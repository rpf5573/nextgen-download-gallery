<?php
/*
Template for the NextGEN Download Gallery
Based on NextGEN Gallery (legacy) basic template (gallery.php) with a few custom additions

These variables are useable :

	$gallery     : Contains all about the gallery
	$images      : Contains all images, path, title
	$pagination  : Contains the pagination content

	$ngg_dlgallery_all_id  : for use in downloading the entire gallery, not only those displayed on one page
*/

if (!defined('ABSPATH')) {
  exit;
}

$current_langauge = apply_filters('wpml_current_language', null);

if (!empty($gallery)) : ?>

  <div class="ngg-galleryoverview ngg-download" id="<?= $gallery->anchor ?>">

    <h3><?= $gallery->title; ?></h3>

    <?php if (!empty($gallery->description)) : ?>
      <p><?= $gallery->description; ?></p>
    <?php endif; ?>

    <?php if (!empty($gallery->show_slideshow)) { ?>
      <div class="slideshowlink">
        <a class="slideshowlink" href="<?= $gallery->slideshow_link ?>">
          <?= $gallery->slideshow_link_text ?>
        </a>
      </div>
    <?php } ?>

    <?php if (!empty($gallery->show_piclens)) { ?>
      <div class="piclenselink">
        <a class="piclenselink" href="<?= $gallery->piclens_link ?>">
          <?php _e('[View with PicLens]', 'nextgen-download-gallery'); ?>
        </a>
      </div>
    <?php } ?>

    <!-- Thumbnails -->
    <form action="<?= esc_url(admin_url('admin-ajax.php')); ?>" method="post" id="<?= $gallery->anchor ?>-download-frm" class="ngg-download-frm">
      <input type="hidden" name="action" value="ngg-download-gallery-zip" />
      <input type="hidden" name="gallery" value="<?= $gallery->title; ?>" />

      <div class="ngg-gallery-item-list-container">
        <?php $i = 0;
        foreach ($images as $image) : ?>

          <div id="ngg-image-<?= $image->pid ?>" class="ngg-gallery-thumbnail-box" <?= $image->style ?>>
            <div class="ngg-gallery-thumbnail">
              <a href="<?= esc_url($image->imageURL); ?>" title="<?= esc_attr($image->description) ?>" <?= $image->thumbcode ?>>
                <?php if (!$image->hidden) { ?>
                  <img title="<?= esc_attr($image->alttext) ?>" alt="<?= esc_attr($image->alttext) ?>" src="<?= esc_url($image->thumbnailURL); ?>" <?= $image->size ?> />
                <?php } ?>
              </a>
              <label><input type="checkbox" name="pid[]" value="<?= esc_attr($image->pid); ?>" /><span><?= esc_html($image->alttext) ?></span></label>
            </div>
          </div>

          <?php if ($image->hidden) continue; ?>
        <?php endforeach; ?>
      </div>

      <hr class="ngg-download-separator" />
      <input class="button ngg-download-deselectall" type="button" value="<?php _e('deselect all', 'nextgen-download-gallery'); ?>" />
      <input class="button ngg-download-download downloadButton" type="submit" value="<?php _e('download selected images', 'nextgen-download-gallery'); ?>" />
      <input class="button ngg-download-all-in-this-page" type="submit" name="download-all-in-this-page" style="display:block;" value="<?php _e('download all images in this page', 'nextgen-download-gallery'); ?>" />
    </form>

    <div id="selected-image-count">
      <?php
      if ($current_langauge === 'zh-hans') { ?>
        选择了 <span></span> 个图像
      <?php
      } elseif ($current_langauge === 'en') { ?>
        <span></span> images are selected
      <?php
      } else { ?>
        <span></span> 개 이미지 선택
      <?php
      }
      ?>
    </div>

    <!-- Pagination -->
    <?= $pagination ?>

  </div>

<?php endif; ?>
